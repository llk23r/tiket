# frozen_string_literal: true

require 'test_helper'

class EventsControllerTest < ActionDispatch::IntegrationTest
  test 'Can load home page' do
    get '/'
    assert_response :success
  end

  test 'Can load sign_in page' do
    get '/users/sign_in/'
    assert_response :success
  end

  test 'Can load sign_up page' do
    get '/users/sign_up/'
    assert_response :success
  end

  test 'Can show event users of event' do
    event_ids = Event.pluck(:id)
    event_ids.map do |event_id|
      get "/events/show.#{event_id}/"
      assert_response :success
    end
  end

  test 'Can load events index' do
    get '/events/index/'
    assert_response :success
  end
end
