# frozen_string_literal: true

require 'test_helper'

class ParticipationTest < ActiveSupport::TestCase
  test 'Can set discounted_ticket_price if user is female' do
    # event_ids = Event.all.pluck(:id)

    User.all.each do |user|
      user.events << Event.all
      participations = user.participations
      participations.each do |participation|
        next unless participation.attending?

        assert participation.discounted_ticket_price.present? == user.is_female?
      end
    end
  end
end
