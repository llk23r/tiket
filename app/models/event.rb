# frozen_string_literal: true

# This model deals with Event. An event has many users through participations.
class Event < ApplicationRecord
  has_many :participations
  has_many :users, through: :participations
end
