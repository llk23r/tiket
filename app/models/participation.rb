# frozen_string_literal: true

# This Model is a result of the `has_many_through` in rails. It's a join table
# of users and events table.
class Participation < ActiveRecord::Base
  belongs_to :user
  belongs_to :event
end
