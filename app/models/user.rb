# frozen_string_literal: true

# This Model deals with establishing relations of User with other Models.
# A user has many events through participations
class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :participations
  has_many :events, through: :participations
end
