# frozen_string_literal: true

# This service delegates participations-responsibilities
class ParticipationService
  attr_accessor :user, :event
  include EventsHelper

  def initialize(user, event)
    @user = user
    @event = event
  end

  def user_event_participation
    event.participations.where(user_id: user&.id)&.last
  end

  def handle_event_participation
    participation = user_event_participation
    return participation if participation.present?

    establish_participation
  end

  def price_after_discount
    super(event)
  end

  def attend_event(participation)
    participation.attending = true
    participation.save!
  end

  def unattend_event(participation)
    participation.attending = false
    participation.save!
  end

  private

  def establish_participation
    event.users << user
    event.participations.where(user_id: user.id).last
  end
end
