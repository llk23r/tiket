# frozen_string_literal: true

# This is a ParticipationsHelper Module to deal with participations related operations
module ParticipationsHelper
  def participation(user, event)
    return nil if user.blank? || event.blank?

    participation_service = ParticipationService.new(user, event)
    [participation_service, participation_service.handle_event_participation]
  end

  def redirectable?(event, user)
    event.blank? && user.blank?
  end
end
