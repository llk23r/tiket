# frozen_string_literal: true

# This is a UsersHelper Module to deal with users related operations
module UsersHelper
  def attending_event?(user, event, participation)
    user&.events.present? &&
      user.events.include?(event) &&
      participation&.attending.present?
  end

  def discount_eligible?(user)
    user.is_female?
  end
end
