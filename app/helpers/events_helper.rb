# frozen_string_literal: true

# This is an EventsHelper Module to deal with events related calculations
module EventsHelper
  def apply_discount(event)
    Discount.discounted_price(event.ticket_fee, 5)
  end

  def show_price(event, user)
    if user.is_female?
      "Attend for discounted ticket fee: #{price_after_discount(event)}"
    else
      "Attend for ticket fee: #{event.ticket_fee}"
    end
  end

  def price_after_discount(event)
    event.ticket_fee - apply_discount(event)
  end

  def event_attendable?(event)
    event.event_date.in_time_zone.utc > DateTime.now.in_time_zone.utc
  end
end
