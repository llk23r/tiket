# frozen_string_literal: true

# EventsController handles what event details should be visible to who, when
class EventsController < ApplicationController
  include EventsHelper
  include UsersHelper
  include ParticipationsHelper

  def index
    @all_events = Event.all.order(created_at: :desc)
  end

  def show
    @event = Event.find(params[:format])
    _, @participation = participation(current_user, @event)
    @discounted_price = apply_discount(@event) if @participation.present? &&
                                                  discount_eligible?(current_user)
  end

  def deregister
    event = Event.find(params[:format])
    redirect_back(fallback_location: root_path) if redirectable?(event, current_user)

    participation_service, participation = participation(current_user, event)
    participation.discounted_ticket_price = nil
    participation_service.unattend_event(participation)
    redirect_back(fallback_location: root_path)
  end

  def register
    event = Event.find(params[:format])
    redirect_back(fallback_location: root_path) if redirectable?(event, current_user)

    participation_service, participation = participation(current_user, event)
    participation.discounted_ticket_price = participation_service.price_after_discount if discount_eligible?(current_user)
    participation_service.attend_event(participation)
    redirect_back(fallback_location: root_path)
  end
end
