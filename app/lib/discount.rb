# frozen_string_literal: true

# Discount module to get the  discounted price given a price
module Discount
  class << self
    def discounted_price(price, percentage)
      (price * percentage) / 100
    end
  end
end
