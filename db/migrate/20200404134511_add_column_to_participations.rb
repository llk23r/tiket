class AddColumnToParticipations < ActiveRecord::Migration[5.2]
  def change
    add_column :participations, :discounted_ticket_price, :money
  end
end
