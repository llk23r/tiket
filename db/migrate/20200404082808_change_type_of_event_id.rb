class ChangeTypeOfEventId < ActiveRecord::Migration[5.2]
  def change
    change_column :participations, :event_id, :integer
  end
end
