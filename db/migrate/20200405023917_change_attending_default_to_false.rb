class ChangeAttendingDefaultToFalse < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:participations, :attending, from: true, to: false)
  end
end
