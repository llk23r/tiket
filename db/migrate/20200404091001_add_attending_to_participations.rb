class AddAttendingToParticipations < ActiveRecord::Migration[5.2]
  def change
    add_column :participations, :attending, :boolean, default: true
  end
end
