class AddNameToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :name, :string
    add_column :events, :event_date, :datetime
    add_column :events, :ticket_fee, :money, default: 0
  end
end
