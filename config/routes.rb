# frozen_string_literal: true

Rails.application.routes.draw do
  # get '/users' => 'users#index', as: :user_root # creates user_root_path
  resource :users, only: %i[index show]

  resource :events, only: %i[index show] do
    member do
      get '/show' => 'events#show', as: :participants
      get '/index' => 'events#index', as: :index
      patch '/deregister' => 'events#deregister', as: :deregister
      patch '/register' => 'events#register', as: :register
    end
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  root to: 'events#index'
end
